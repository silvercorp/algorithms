/*
 * Factorials
 * Follow me @silvercorp
 * Suscribe www.youtube.com/user/silvercorp
*/

//Given a string, reverse only the vowels present in it and print the resulting string
/*
 * geeksforgeeks = geeksforgeeks
 * practice = prectica
 * ransomware = rensamwora
 */

function reverseVowels(word) {

  var vowels = ['a', 'e', 'i', 'o', 'u'];
  var temp = new Array();

  var letters = word.split('');
  console.log("Initial Word: " + letters);
  var totalLetters = letters.length;

  for (var i = totalLetters; i >= 0; i--) {
    if ( vowels.indexOf(letters[i]) > -1 ) {
      temp.push(letters[i]);
    }
  }

  var k = 0;

  for (var j = 0; j < totalLetters; j++) {
    if ( vowels.indexOf(letters[j]) > -1 ) {
      letters[j] = temp[k];
      k++;
    }
  }

  console.log("Reverse word : " + letters);

}


reverseVowels('practice');
reverseVowels('geeksforgeeks');
reverseVowels('ransomware');