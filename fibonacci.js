/*
 * Factorials
 * Follow me @silvercorp
 * Suscribe www.youtube.com/user/silvercorp
*/

/*
* Write a function that computes the list of the first 100 Fibonacci numbers. By definition, the first two numbers in the Fibonacci
* sequence are 0 and 1, and each subsequent number is the sum of the previous two. As an example, here are the first 10 Fibonnaci numbers:
* 0, 1, 1, 2, 3, 5, 8, 13, 21, and 34.
*/


/* 1, 1, 2, 3, 5, 8, 13
 Classic algorithm
 */
function fibonacci(n) {

  if (n == 1 || n == 2) {
    return 1;
  } else {
    return fibonacci(n - 1) + fibonacci(n - 2)
  }
}

/* Dynamic algorithm
 * [1, 1, X, X, X]
 */
function dfibonacci(n) {
  var numbers = [];
  numbers[0] = 1;
  numbers[1] = 1;

  for (var i = 2; i < n; i++) {
    numbers[i] = numbers[i - 1] + numbers[i - 2];
  }

  return numbers[n - 1];
}

var classic = fibonacci(6);
console.log(classic);

var dynamic = dfibonacci(6);
console.log(dynamic);