/*
 * Factorials
 * Follow me @silvercorp
 * Suscribe www.youtube.com/user/silvercorp
*/

function factorial(n) {
  if ( n >= 1) {
    return n * factorial(n - 1);
  } else {
    return 1;
  }
}


var n = 5;
var fact = factorial(n);
console.log(fact);