/*
 * Factorials
 * Follow me @silvercorp
 * Suscribe www.youtube.com/user/silvercorp
*/

/**
 * Given a 32-bit signed integer, reverse digits of an integer.
 * @param {number} x
 * @return {number}
 */
var reverse = function(x) {

   var reverse = parseInt(x.toString().split('').reverse().join(''));

   if(reverse > Math.pow(2,31)) return 0;

   return reverse * Math.sign(x);

};